package br.ufes.exercicio2;

import br.ufes.exercicio2.SanduicheBuilder.AmericanoBuilder;
import br.ufes.exercicio2.SanduicheBuilder.Diretor;
import br.ufes.exercicio2.SanduicheBuilder.HamburguerBuilder;
import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.Sanduiche;

public class Principal {

    public static void main(String args[]) {

        Diretor diretor = new Diretor();
        
        Sanduiche sanduiche = diretor.contruir(new AmericanoBuilder());
        System.out.println(sanduiche.getDescricao());
        System.out.println(sanduiche.getPreco());
        
        System.out.println("");
        
        sanduiche = new Ingrediente(sanduiche, "Ovo frito", 1.0);
        System.out.println(sanduiche.getDescricao());
        System.out.println(sanduiche.getPreco());
        
        System.out.println("");
        
        sanduiche = new Ingrediente(sanduiche, "Banana frita", 1.0);
        System.out.println(sanduiche.getDescricao());
        System.out.println(sanduiche.getPreco());
    }

}
