package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.SanduicheConcreto;

public class CapreseBuilder extends Builder {

    @Override
    public void reset() {
        this.sanduiche = new SanduicheConcreto("Caprese", 0);
    }
    
    @Override
    public void adicionarPao() {
        this.sanduiche = new Ingrediente(sanduiche, "Focaccia", 2.0);
    }

    @Override
    public void adicionarIngrediente() {
        this.sanduiche = new Ingrediente(sanduiche, "Tomate", 0.3);
        this.sanduiche = new Ingrediente(sanduiche, "Mozzarella", 2.0);
        this.sanduiche = new Ingrediente(sanduiche, "Manjericão", 0.5);
    }

}
