package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.SanduicheConcreto;

public class MistoQuenteBuilder extends Builder {

    @Override
    public void reset() {
        this.sanduiche = new SanduicheConcreto("Misto Quente", 0);
    }
    
    @Override
    public void adicionarIngrediente() {
        this.sanduiche = new Ingrediente(sanduiche, "Pão de Forma", 0.5);
    }

    @Override
    public void adicionarPao() {
        this.sanduiche = new Ingrediente(sanduiche, "Presunto", 1.0);
        this.sanduiche = new Ingrediente(sanduiche, "Queijo", 1.0);
    }

}
