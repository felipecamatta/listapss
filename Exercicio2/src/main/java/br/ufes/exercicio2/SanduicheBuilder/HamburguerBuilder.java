package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.SanduicheConcreto;

public class HamburguerBuilder extends Builder {

    @Override
    public void reset() {
        this.sanduiche = new SanduicheConcreto("Hamburguer", 0);
    }

    @Override
    public void adicionarPao() {
        this.sanduiche = new Ingrediente(sanduiche, "Pão de Hamburguer", 0.5);
    }

    @Override
    public void adicionarIngrediente() {
        this.sanduiche = new Ingrediente(sanduiche, "Carne", 3.0);
    }

}
