package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.SanduicheConcreto;

public class CachorroQuenteBuilder extends Builder {
    
    @Override
    public void reset() {
        this.sanduiche = new SanduicheConcreto("Cachorro Quente", 0);
    }
    
    @Override
    public void adicionarPao() {
        this.sanduiche = new Ingrediente(sanduiche, "Pão Sovado", 0.5);
    }
    
    @Override
    public void adicionarIngrediente() {        
        this.sanduiche = new Ingrediente(sanduiche, "Salsicha", 1.0);
    }
    
}
