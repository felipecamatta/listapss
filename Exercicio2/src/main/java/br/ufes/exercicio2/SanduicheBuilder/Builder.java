package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Sanduiche;

public abstract class Builder {

    protected Sanduiche sanduiche;

    public abstract void adicionarIngrediente();

    public abstract void adicionarPao();

    public abstract void reset();

    public final Sanduiche getSanduiche() {
        return this.sanduiche;
    }

}
