package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.SanduicheConcreto;

public class BeiruteBuilder extends Builder {

    @Override
    public void reset() {
        this.sanduiche = new SanduicheConcreto("Beirute", 0);
    }
    
    @Override
    public void adicionarPao() {
        this.sanduiche = new Ingrediente(sanduiche, "Pão Sírio", 1.5);
    }
    
    @Override
    public void adicionarIngrediente() {        
        this.sanduiche = new Ingrediente(sanduiche, "Rosbife", 3.0);
        this.sanduiche = new Ingrediente(sanduiche, "Queijo", 1.0);
        this.sanduiche = new Ingrediente(sanduiche, "Alface", 0.3);
        this.sanduiche = new Ingrediente(sanduiche, "Tomate", 0.3);
        this.sanduiche = new Ingrediente(sanduiche, "Ovo frito", 1.0);
    }
    
}
