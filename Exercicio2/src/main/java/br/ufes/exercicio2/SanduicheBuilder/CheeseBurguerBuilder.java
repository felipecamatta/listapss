package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.SanduicheConcreto;

public class CheeseBurguerBuilder extends Builder {

    @Override
    public void reset() {
        this.sanduiche = new SanduicheConcreto("CheeseBurguer", 0);
    }
    
    @Override
    public void adicionarPao() {
        this.sanduiche = new Ingrediente(sanduiche, "Pão de Hamburguer", 0.5);
    }

    @Override
    public void adicionarIngrediente() {
        this.sanduiche = new Ingrediente(sanduiche, "Carne de Hamburguer", 3.0);
        this.sanduiche = new Ingrediente(sanduiche, "Queijo", 1.0);
    }

}
