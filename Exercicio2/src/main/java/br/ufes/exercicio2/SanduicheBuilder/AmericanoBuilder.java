package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Ingrediente;
import br.ufes.exercicio2.SanduicheDecorator.SanduicheConcreto;

public class AmericanoBuilder extends Builder {

    @Override
    public void reset() {
        this.sanduiche = new SanduicheConcreto("Americano", 0);
    }

    @Override
    public void adicionarPao() {
        this.sanduiche = new Ingrediente(sanduiche, "Pão de Hamburguer", 0.5);
    }

    @Override
    public void adicionarIngrediente() {
        this.sanduiche = new Ingrediente(sanduiche, "Presunto", 1.0);
        this.sanduiche = new Ingrediente(sanduiche, "Queijo", 1.0);
        this.sanduiche = new Ingrediente(sanduiche, "Ovo", 1.0);
        this.sanduiche = new Ingrediente(sanduiche, "Alface", 0.3);
        this.sanduiche = new Ingrediente(sanduiche, "Tomate", 0.3);
        this.sanduiche = new Ingrediente(sanduiche, "Maionese", 0.2);
    }

}
