package br.ufes.exercicio2.SanduicheBuilder;

import br.ufes.exercicio2.SanduicheDecorator.Sanduiche;

public class Diretor {

    public Sanduiche contruir(Builder builder) {
        builder.reset();
        builder.adicionarPao();
        builder.adicionarIngrediente();
        return builder.getSanduiche();
    }

}
