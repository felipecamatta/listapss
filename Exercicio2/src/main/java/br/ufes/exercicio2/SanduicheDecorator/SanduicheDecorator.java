package br.ufes.exercicio2.SanduicheDecorator;

public abstract class SanduicheDecorator extends Sanduiche {

    private Sanduiche elementoDecorado;

    public SanduicheDecorator(Sanduiche elementoDecorado, String descricao, double preco) {
        super(descricao, preco);
        this.elementoDecorado = elementoDecorado;
    }

    public Sanduiche getElemento() {
        return elementoDecorado;
    }

}
