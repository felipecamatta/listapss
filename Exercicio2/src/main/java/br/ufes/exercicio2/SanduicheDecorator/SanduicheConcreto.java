package br.ufes.exercicio2.SanduicheDecorator;

public class SanduicheConcreto extends Sanduiche {

    public SanduicheConcreto() {
    }

    public SanduicheConcreto(String descricao, double preco) {
        super(descricao, preco);
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public double getPreco() {
        return this.preco;
    }

}
