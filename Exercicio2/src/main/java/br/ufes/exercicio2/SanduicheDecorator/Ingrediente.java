package br.ufes.exercicio2.SanduicheDecorator;

public class Ingrediente extends SanduicheDecorator {

    public Ingrediente(Sanduiche elementoDecorado, String descricao, double preco) {
        super(elementoDecorado, descricao, preco);
    }

    @Override
    public String getDescricao() {
        return this.getElemento().getDescricao() + "\n" + this.descricao;
    }

    @Override
    public double getPreco() {
        return this.getElemento().getPreco() + this.preco;
    }

}
