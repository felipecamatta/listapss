package br.ufes.exercicio2.SanduicheDecorator;

public abstract class Sanduiche {

    protected String descricao = "";
    protected double preco;
    
    public Sanduiche() {        
    }

    public Sanduiche(String descricao, double preco) {
        this.descricao = descricao;
        this.preco = preco;
    }

    public abstract String getDescricao();

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public abstract double getPreco();

    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return descricao + "\t R$" + preco + '\n';
    }

}
