package br.ufes.exercicio1;

/**
 *
 * @author Felipe
 */
public class UtilitarioString {

    private static UtilitarioString instancia = null;

    private UtilitarioString() {
    }

    public static UtilitarioString getInstancia() {
        if (instancia == null) {
            instancia = new UtilitarioString();
        }
        return instancia;
    }

    public String substituirCaracter(String texto, String palavra, CharSequence simbolo) {
        String substituir = simbolo.toString().repeat(palavra.length());

        texto = texto.replace(palavra, substituir);

        return texto;
    }

    public String substituirPorOutroTexto(String texto, String palavra, String novoTexto) {
        if (texto.contains(palavra)) {
            return novoTexto;
        } else {
            return texto;
        }
    }

}
