package br.ufes.exercicio1.business.substituir.marca;

import br.ufes.exercicio1.UtilitarioString;

public abstract class SubstituirMarca {

    private String palavra;

    public String substituir(String texto) {
        return UtilitarioString.getInstancia().substituirCaracter(texto, this.getPalavra(), "*");
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

}
