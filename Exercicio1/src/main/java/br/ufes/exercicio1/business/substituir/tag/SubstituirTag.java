package br.ufes.exercicio1.business.substituir.tag;

import br.ufes.exercicio1.UtilitarioString;

public class SubstituirTag {

    private String palavra;

    public String substituir(String texto) {
        return UtilitarioString.getInstancia().substituirPorOutroTexto(texto, this.getPalavra(),
                "Mensagem removida por conter conteúdo não autorizado");
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

}
