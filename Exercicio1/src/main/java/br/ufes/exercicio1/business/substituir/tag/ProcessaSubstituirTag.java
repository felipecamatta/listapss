package br.ufes.exercicio1.business.substituir.tag;

import br.ufes.exercicio1.business.substituir.Processador;
import java.util.ArrayList;
import java.util.List;

public class ProcessaSubstituirTag implements Processador {

    private List<SubstituirTag> listaSubstituir = new ArrayList<>();

    public ProcessaSubstituirTag() {
        listaSubstituir.add(new PSubstituir());
        listaSubstituir.add(new AHrefSubstituir());
        listaSubstituir.add(new TableSubstituir());
        listaSubstituir.add(new ImgSubstituir());
    }

    public String executa(String texto) {
        for (SubstituirTag substituir : listaSubstituir) {
            texto = substituir.substituir(texto);
        }

        return texto;
    }

}
