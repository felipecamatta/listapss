package br.ufes.exercicio1.business.substituir.marca;

import br.ufes.exercicio1.business.substituir.Processador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Felipe
 */
public class ProcessaSubstituirMarca implements Processador {

    private List<SubstituirMarca> listaSubstituir = new ArrayList<>();

    public ProcessaSubstituirMarca() {
        listaSubstituir.add(new MicrosoftSubstituir());
        listaSubstituir.add(new FacebookSubstituir());
        listaSubstituir.add(new IBMSubstituir());
        listaSubstituir.add(new AppleSubstituir());
    }

    public String executa(String texto) {
        for (SubstituirMarca substituir : listaSubstituir) {
            texto = substituir.substituir(texto);
        }

        return texto;
    }

}
