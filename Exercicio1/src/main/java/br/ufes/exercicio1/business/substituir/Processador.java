package br.ufes.exercicio1.business.substituir;

public interface Processador {
    
    public String executa(String texto);
    
}
