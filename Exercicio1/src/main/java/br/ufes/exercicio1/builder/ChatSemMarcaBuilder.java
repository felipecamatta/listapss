package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.business.substituir.marca.ProcessaSubstituirMarca;

public class ChatSemMarcaBuilder extends Builder {

    @Override
    public void adicionarProcessador() {
        this.chat.setProcessador(new ProcessaSubstituirMarca());
    }   
    
}
