package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.business.substituir.tag.ProcessaSubstituirTag;

public class ChatSemTagBuilder extends Builder {

    @Override
    public void adicionarProcessador() {
        this.chat.setProcessador(new ProcessaSubstituirTag());
    }

}
