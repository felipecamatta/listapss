package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.proxy.SalaChatProxy;

public abstract class Builder {
    
    protected SalaChatProxy chat;
    
    public void reset() {
        this.chat = new SalaChatProxy();
    }
    
    public final SalaChatProxy getChat() {
        return this.chat;
    }
    
    public abstract void adicionarProcessador();
    
}
