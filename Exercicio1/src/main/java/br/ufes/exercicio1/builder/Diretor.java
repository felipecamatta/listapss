package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.proxy.SalaChatProxy;

public class Diretor {
    
    public SalaChatProxy construir(Builder builder) {
        builder.reset();
        builder.adicionarProcessador();
        return builder.getChat();
    }
    
}
