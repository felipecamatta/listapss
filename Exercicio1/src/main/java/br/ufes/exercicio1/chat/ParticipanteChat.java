package br.ufes.exercicio1.chat;

public class ParticipanteChat extends Participante {

    private String name;

    public ParticipanteChat(MediatorChat chatMediator, String participantName) {
        this.mediator = chatMediator;
        this.name = participantName;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void enviar(String message) {
        this.mediator.enviar(this, message);
    }

    @Override
    public void receber(String message, Participante participante) {
        System.out.println(participante.getName() + ": " + message);
    }

}
