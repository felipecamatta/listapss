package br.ufes.exercicio1.chat;

public interface MediatorChat {
    
    public void enviar(Participante participante, String mensagem);
    
    public Participante criarParticipante(MediatorChat mediatorChat, String name);
    
}
