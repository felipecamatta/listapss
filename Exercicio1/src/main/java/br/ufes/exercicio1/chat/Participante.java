package br.ufes.exercicio1.chat;

public abstract class Participante {
    
    protected MediatorChat mediator;

    public abstract String getName();

    public abstract void enviar(String message);

    public abstract void receber(String message, Participante participante);

}
