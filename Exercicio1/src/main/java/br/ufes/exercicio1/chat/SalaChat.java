package br.ufes.exercicio1.chat;

import java.util.ArrayList;
import java.util.List;

public class SalaChat implements MediatorChat {

    List<Participante> participants;

    public SalaChat() {
        this.participants = new ArrayList<>();
    }

    @Override
    public void enviar(Participante participante, String mensagem) {
        participants.forEach(p -> {
            if (!p.equals(participante)) {
                p.receber(mensagem, participante);
            }
        });
    }

    @Override
    public Participante criarParticipante(MediatorChat mediatorChat, String name) {
        Participante participante = new ParticipanteChat(mediatorChat, name);
        this.participants.add(participante);
        return participante;
    }

}
