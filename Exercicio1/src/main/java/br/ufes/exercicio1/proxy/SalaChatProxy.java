package br.ufes.exercicio1.proxy;

import br.ufes.exercicio1.business.substituir.Processador;
import br.ufes.exercicio1.chat.MediatorChat;
import br.ufes.exercicio1.chat.Participante;
import br.ufes.exercicio1.chat.SalaChat;

public class SalaChatProxy implements MediatorChat {

    SalaChat salaChat;
    Processador processador;

    public SalaChatProxy() {
        this.salaChat = new SalaChat();
    }

    public SalaChatProxy(Processador processador) {
        this.salaChat = new SalaChat();
        this.processador = processador;
    }

    @Override
    public void enviar(Participante participante, String mensagem) {
        String mensagemProcessada = processador.executa(mensagem);
        this.salaChat.enviar(participante, mensagemProcessada);
    }

    @Override
    public Participante criarParticipante(MediatorChat mediatorChat, String name) {
        return this.salaChat.criarParticipante(mediatorChat, name);
    }

    public void setProcessador(Processador processador) {
        this.processador = processador;
    }

}
