package br.ufes.exercicio1;

import br.ufes.exercicio1.builder.ChatSemMarcaBuilder;
import br.ufes.exercicio1.builder.ChatSemTagBuilder;
import br.ufes.exercicio1.builder.Diretor;
import br.ufes.exercicio1.chat.MediatorChat;
import br.ufes.exercicio1.chat.Participante;

public class Principal {

    public static void main(String args[]) {
        
        Diretor diretor = new Diretor();
        
        System.out.println("Exemplo de chat sem marcas\n");
        
        MediatorChat salaChatSemMarca = diretor.construir(new ChatSemMarcaBuilder());
        Participante p1 = salaChatSemMarca.criarParticipante(salaChatSemMarca, "João");
        Participante p2 = salaChatSemMarca.criarParticipante(salaChatSemMarca, "Maria");
        p1.enviar("Olá Maria! Como esta seu trabalho ai na IBM?");
        p2.enviar("Olá João! Aqui na IBM está tudo certo! E o seu trabalho no Facebook, como está?");
        p1.enviar("Está tudo certo também, mas recebi uma proposta da Microsoft");
        p2.enviar("Que ótimo João!!! A Microsoft é uma excelente empresa também!");
        p2.enviar("Estou querendo ir para a Apple");
        p1.enviar("Que máximo! Espero que você consiga!");
        
        System.out.println("\n-----------------------------------------------");
        
        System.out.println("Exemplo de chat sem tags html\n");
        
        MediatorChat salaChatSemTag = diretor.construir(new ChatSemTagBuilder());
        Participante p3 = salaChatSemTag.criarParticipante(salaChatSemTag, "Pedro");
        Participante p4 = salaChatSemTag.criarParticipante(salaChatSemTag, "Jorge");
        p3.enviar("Fala Jorge, beleza? Você poderia me mandar o HTML daquela tabela do site da empresa?");
        p4.enviar("Opa Pedro. Mando sim!");
        p4.enviar("<table><tr>.....");
        p3.enviar("Vish, parece que o conteúdo não é autorizado!");
        p3.enviar("Tenta me enviar aquela imagem e paragrafos da página de inicio então");
        p4.enviar("<a href=...");
        p4.enviar("<img src....");
        p4.enviar("<p>...</p>");
        p3.enviar("Vish, deixa quieto, o chat está removendo os códigos");
        
    }

}
