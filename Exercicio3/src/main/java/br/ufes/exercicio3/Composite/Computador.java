package br.ufes.exercicio3.Composite;

import java.util.ArrayList;
import java.util.List;

public class Computador extends Component {

    List<Component> componentes;

    public Computador() {
        super();
        this.componentes = new ArrayList<>();
    }

    public Computador(String nome, double valor) {
        super(nome, valor);
        this.componentes = new ArrayList<>();
    }

    public void add(Component componente) {
        this.componentes.add(componente);
    }

    public void remove(Component componente) {
        this.componentes.remove(componente);
    }

    @Override
    public String getNome() {
        String descricaoCompleta = this.nome;
        for (Component component : componentes) {
            descricaoCompleta += "\n" + component.getNome();
        }
        return descricaoCompleta;
    }

    @Override
    public double getValor() {
        double valorTotal = this.valor;
        for (Component component : componentes) {
            valorTotal += component.getValor();
        }
        return valorTotal;
    }

}
