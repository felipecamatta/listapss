package br.ufes.exercicio3.Composite;

public abstract class Component {

    protected String nome = "";
    protected double valor;

    public Component() {
    }

    public Component(String nome) {
        this.nome = nome;
    }

    public Component(String nome, double valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public abstract String getNome();

    public abstract double getValor();

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

}
