package br.ufes.exercicio3.Composite;

public class Peca extends Component {

    public Peca(String nome, double valor) {
        super(nome, valor);
    }

    public String getNome() {
        return nome;
    }

    public double getValor() {
        return valor;
    }

}
