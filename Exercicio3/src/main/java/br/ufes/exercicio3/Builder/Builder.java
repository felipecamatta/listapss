package br.ufes.exercicio3.Builder;

import br.ufes.exercicio3.Composite.Component;
import br.ufes.exercicio3.Composite.Computador;

public abstract class Builder {
    
    protected Computador component;
    
    public abstract void adicionarPeca();
    
    public void reset() {
        this.component = new Computador();
    }
    
    public final Component getComputador() {
        return this.component;
    }
    
}
