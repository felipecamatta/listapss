package br.ufes.exercicio3.Builder;

import br.ufes.exercicio3.Composite.Peca;

public class PCSimplesBuilder extends Builder {

    @Override
    public void adicionarPeca() {
        this.component.setNome("PC Simples");
        this.component.add(new Peca("Gabinete Simples", 100));
        this.component.add(new Peca("Placa mãe Intel", 300));
        this.component.add(new Peca("Mémoriam Ram 4GB", 100));
        this.component.add(new Peca("HD 250GB", 100));
        this.component.add(new Peca("Fonte 200W", 150));
        this.component.add(new Peca("Procesador Intel Core Pentium", 350));
    }
    
}
