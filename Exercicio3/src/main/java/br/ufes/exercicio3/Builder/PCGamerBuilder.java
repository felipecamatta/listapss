package br.ufes.exercicio3.Builder;

import br.ufes.exercicio3.Composite.Peca;

public class PCGamerBuilder extends Builder {

    @Override
    public void adicionarPeca() {
        this.component.setNome("PC GAMER");
        this.component.add(new Peca("Gabinete Gamer RGB", 100));
        this.component.add(new Peca("Placa mãe Intel Extreme", 1000));
        this.component.add(new Peca("Mémoriam Ram 32GB", 500));
        this.component.add(new Peca("SSD 960GB", 500));
        this.component.add(new Peca("HD 2TB", 400));
        this.component.add(new Peca("Fonte 600W", 350));
        this.component.add(new Peca("Procesador Intel Core i9", 2000));
    }
    
}
