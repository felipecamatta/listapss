package br.ufes.exercicio3.Builder;

import br.ufes.exercicio3.Composite.Component;

public class Diretor {

    public Component construir(Builder builder) {
        builder.reset();
        builder.adicionarPeca();
        return builder.getComputador();
    }

}
