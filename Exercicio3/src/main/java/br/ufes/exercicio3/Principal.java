package br.ufes.exercicio3;

import br.ufes.exercicio3.Builder.Diretor;
import br.ufes.exercicio3.Builder.PCGamerBuilder;
import br.ufes.exercicio3.Builder.PCSimplesBuilder;
import br.ufes.exercicio3.Composite.Component;

public class Principal {

    public static void main(String args[]) {

        Diretor diretor = new Diretor();

        Component computadorSimples = diretor.construir(new PCSimplesBuilder());
        System.out.println(computadorSimples.getNome());
        System.out.println("R$" + computadorSimples.getValor());
        
        System.out.println("");
        
        Component computadorGamer = diretor.construir(new PCGamerBuilder());
        System.out.println(computadorGamer.getNome());
        System.out.println("R$" + computadorGamer.getValor());

    }

}
